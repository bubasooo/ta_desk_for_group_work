package com.ru.tinkoff.tadeskforgroupwork.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ru.tinkoff.tadeskforgroupwork.desk.Desk;
import com.ru.tinkoff.tadeskforgroupwork.form.Form;
import com.ru.tinkoff.tadeskforgroupwork.user.User;
import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "task")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Task implements Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "creates")
    private LocalDate creates;

    @NotNull
    @Column(name = "changes")
    private LocalDate changes;

    @NotNull
    @Column(name = "tag", length = 20)
    private String tag;

    @NotNull
    @ManyToOne
    @JoinColumn(name="desk_id" , nullable = false)
    private Desk desk;

    @NotNull
    @ManyToOne
    @JoinColumn(name="users_id", nullable = false)
    private User usersTask;

}