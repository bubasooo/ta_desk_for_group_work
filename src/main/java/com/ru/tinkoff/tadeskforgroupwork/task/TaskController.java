package com.ru.tinkoff.tadeskforgroupwork.task;

import com.ru.tinkoff.tadeskforgroupwork.task.dto.CreatingTaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @PostMapping
    public Task save(@RequestBody CreatingTaskDto taskDto) {
        return taskService.save(taskDto);
    }

    @PatchMapping("/edit/name/{taskTag}")
    public Task changeName(@PathVariable String taskTag, @RequestBody CreatingTaskDto taskDto) {
        return taskService.changeName(taskTag, taskDto.getName());
    }

    @PatchMapping("/edit/description/{taskTag}")
    public Task changeDescription(@PathVariable String taskTag, @RequestBody CreatingTaskDto taskDto) {
        return taskService.changeDescription(taskTag, taskDto.getDescripion());
    }

    @PatchMapping("/edit/tag/{oldTag}")
    public Task updateTag(@PathVariable String oldTag, @RequestBody CreatingTaskDto taskDto) {
        return taskService.updateTag(oldTag, taskDto.getTag());
    }

    @PatchMapping("/edit/user/{taskTag}")
    public Task changeUserToDoingTask(@PathVariable String taskTag, @RequestBody CreatingTaskDto taskDto) {
        return taskService.changeUserToDoingTask(taskTag, taskDto.getUserId());
    }

    @DeleteMapping("/delete/{tagName}")
    public Task deleteTask(@PathVariable String tagName) {
        return taskService.deleteTask(tagName);
    }
}

