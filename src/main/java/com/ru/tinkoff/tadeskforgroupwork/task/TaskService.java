package com.ru.tinkoff.tadeskforgroupwork.task;


import com.ru.tinkoff.tadeskforgroupwork.core.configuration.exeptions.FieldIsRequiredException;
import com.ru.tinkoff.tadeskforgroupwork.desk.Desk;
import com.ru.tinkoff.tadeskforgroupwork.desk.DeskRepository;
import com.ru.tinkoff.tadeskforgroupwork.task.dto.CreatingTaskDto;
import com.ru.tinkoff.tadeskforgroupwork.user.User;
import com.ru.tinkoff.tadeskforgroupwork.user.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final UserService userService;
    private final TaskRepository taskRepository;
    private final DeskRepository deskService;

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByTag(String tag) {
        return taskRepository.findTaskByTag(tag);
    }

    public Task save(CreatingTaskDto taskDto) {

        User userDoingTask = userService.getUserById(Optional.ofNullable(taskDto.getUserId()).orElseThrow(
                () -> new FieldIsRequiredException("Field userId is required"))
        );
        Desk desk = deskService.findDeskById(Optional.ofNullable(taskDto.getDeskId()).orElseThrow(
                () -> new FieldIsRequiredException("Field deskId is required")
        ));

        Task task = Task.builder()
                .name(Optional.ofNullable(taskDto.getName()).orElse("New Task"))
                .description(Optional.ofNullable(taskDto.getDescripion()).orElse("No Description"))
                .tag(Optional.ofNullable(taskDto.getTag()).orElseThrow(
                        () -> new FieldIsRequiredException("Field tag is required"))
                )
                .creates(LocalDate.now())
                .changes(LocalDate.now())
                .usersTask(userDoingTask)
                .desk(desk)
                .build();

        return taskRepository.save(task);
    }

    public Task changeName(String tag, String newName) {
        Task taskForChanging = taskRepository.findTaskByTag(tag);

        if (Optional.ofNullable(newName).isPresent()) {
            taskForChanging.setName(newName);
            taskForChanging.setChanges(LocalDate.now());
        }

        return taskRepository.save(taskForChanging);
    }

    public Task changeDescription(String tag, String newDescription) {
        Task taskForChanging = taskRepository.findTaskByTag(tag);

        if (Optional.ofNullable(newDescription).isPresent()) {
            taskForChanging.setDescription(newDescription);
            taskForChanging.setChanges(LocalDate.now());
        }

        return taskRepository.save(taskForChanging);
    }

    public Task updateTag(String oldTag, String newTag) {
        Task taskForChanging = taskRepository.findTaskByTag(oldTag);


        taskForChanging.setTag(Optional.ofNullable(newTag).orElseThrow(
                () -> new FieldIsRequiredException("Field tag is required"))
        );
        taskForChanging.setChanges(LocalDate.now());


        return taskRepository.save(taskForChanging);
    }

    public Task changeUserToDoingTask(String tag, Long userId) {
        Task taskForChanging = taskRepository.findTaskByTag(tag);

        User newUserToDoing = userService.getUserById(Optional.ofNullable(userId).orElseThrow(
                () -> new FieldIsRequiredException("Field userId is required"))
        );

        taskForChanging.setUsersTask(newUserToDoing);
        taskForChanging.setChanges(LocalDate.now());

        return taskRepository.save(taskForChanging);
    }

    public Task deleteTask(String tag) {
        Task taskForDelete = taskRepository.findTaskByTag(tag);

        taskRepository.delete(taskForDelete);

        return taskForDelete;

    }
}
