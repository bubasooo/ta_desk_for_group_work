package com.ru.tinkoff.tadeskforgroupwork.task.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingTaskDto {
    private String name;
    private String descripion;
    private String tag;
    private Long deskId;
    private Long userId;

}
