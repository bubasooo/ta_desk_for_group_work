package com.ru.tinkoff.tadeskforgroupwork.core.configuration.exeptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FieldIsRequiredException extends RuntimeException {

    public FieldIsRequiredException() {

    }

    public FieldIsRequiredException(String message) {
        super(message);
    }

    public FieldIsRequiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldIsRequiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
