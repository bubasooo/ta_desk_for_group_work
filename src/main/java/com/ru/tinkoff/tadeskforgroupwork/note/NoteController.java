package com.ru.tinkoff.tadeskforgroupwork.note;

import com.ru.tinkoff.tadeskforgroupwork.note.dto.CreatingNoteDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/note")
@RequiredArgsConstructor
public class NoteController {

    private final NoteService noteService;

    @GetMapping
    public List<Note> findAll() {
        return noteService.findAll();
    }

    @GetMapping("/tag/{noteTag}/get_connected_notes")
    public List<Note> getConnectedNotes(@PathVariable String noteTag) {
        return noteService.getConnectedNotes(noteTag);
    }

    @GetMapping("/tag/{noteTag}")
    public Note findByTag(@PathVariable String noteTag) {
        return noteService.findByTag(noteTag);
    }

    @PostMapping
    public Note save(@RequestBody CreatingNoteDto noteDto) {
        return noteService.save(noteDto);
    }

    @PatchMapping("/edit/tag/{noteTag}")
    public Note changeTag(@PathVariable String noteTag, @RequestBody CreatingNoteDto noteDto) {
        return noteService.changeTag(noteTag, noteDto.getTag());
    }

    @PatchMapping("/edit/name/{noteTag}")
    public Note changeName(@PathVariable String noteTag, @RequestBody CreatingNoteDto noteDto) {
        return noteService.changeName(noteTag, noteDto.getName());
    }

    @PatchMapping("/edit/content/{noteTag}")
    public Note changeContent(@PathVariable String noteTag, @RequestBody CreatingNoteDto noteDto) {
        return noteService.changeContent(noteTag, noteDto.getContent());
    }

    @PostMapping("/edit/notes_relations/{startNoteTag}")
    public Note connectNotes(@PathVariable String startNoteTag, @RequestBody CreatingNoteDto endNoteTag) {
        return noteService.connectNotes(startNoteTag, endNoteTag.getTag());
    }

    @DeleteMapping("/delete/{noteTag}")
    public Note deleteNote(@PathVariable String noteTag) {
        return noteService.deleteNote(noteTag);
    }
}

