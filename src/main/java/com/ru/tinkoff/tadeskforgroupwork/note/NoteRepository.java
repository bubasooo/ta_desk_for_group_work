package com.ru.tinkoff.tadeskforgroupwork.note;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note,Long> {

    Note findNoteByTag(String tag);

    @Modifying
    @Query(value = "insert into note_note(note_start_id, note_end_id) values(:startId, :endId);",nativeQuery = true)
    void connectNotes(Long startId, Long endId);
}
