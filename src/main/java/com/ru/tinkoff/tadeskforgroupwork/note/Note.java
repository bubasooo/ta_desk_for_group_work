package com.ru.tinkoff.tadeskforgroupwork.note;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ru.tinkoff.tadeskforgroupwork.form.Form;
import com.ru.tinkoff.tadeskforgroupwork.user.User;
import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "note")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Note implements Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "name", length = 20)
    private String name;

    @NotNull
    @Column(name = "content")
    private String content;

    @NotNull
    @Column(name = "tag", length = 20)
    private String tag;

    @NotNull
    @Column(name = "creates")
    private LocalDate creates;

    @NotNull
    @Column(name = "changes")
    private LocalDate changes;

    @NotNull
    @ManyToOne
    @JoinColumn(name="users_id")
    private User usersNote;

    @JsonIgnore
    @ToString.Exclude //???
    @ManyToMany
    @JoinTable(
            name = "note_note",
            joinColumns = @JoinColumn(name = "note_start_id"),
            inverseJoinColumns = @JoinColumn(name = "note_end_id"))
    private List<Note> neighborsNotes;

    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(mappedBy ="neighborsNotes") //????
    private List<Note> notes;

}