package com.ru.tinkoff.tadeskforgroupwork.note.dto;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatingNoteDto {
    private String name;
    private String content;
    private String tag;
//    private List<Long> neighborNotesId;
    private String usersNickname;
}
