package com.ru.tinkoff.tadeskforgroupwork.note;

import com.ru.tinkoff.tadeskforgroupwork.core.configuration.exeptions.FieldIsRequiredException;
import com.ru.tinkoff.tadeskforgroupwork.note.dto.CreatingNoteDto;
import com.ru.tinkoff.tadeskforgroupwork.user.User;
import com.ru.tinkoff.tadeskforgroupwork.user.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NoteService {

    private final NoteRepository noteRepository;
    private final UserService userService;

    public List<Note> findAll() {
        return noteRepository.findAll();
    }

    @Transactional
    public Note save(CreatingNoteDto noteDto) {

        User user = userService.getByNickname(Optional.ofNullable(noteDto.getUsersNickname()).orElseThrow(
                () -> new FieldIsRequiredException("Field nickname is required")
        ));

        Note note = Note.builder()
                .name(Optional.ofNullable(noteDto.getName()).orElse("New Note"))
                .content(Optional.ofNullable(noteDto.getContent()).orElse("No content"))
                .tag(Optional.ofNullable(noteDto.getTag()).orElseThrow(
                        () -> new FieldIsRequiredException("Field tag is required")
                ))
                .creates(LocalDate.now())
                .changes(LocalDate.now())
                .usersNote(user)
                .build();
        return noteRepository.save(note);
    }

    public List<Note> getConnectedNotes(String tag) {
        Note note = this.findByTag(tag);
        return note.getNeighborsNotes();
    }

    public Note findByTag(String tag) {
        return noteRepository.findNoteByTag(tag);
    }

    @Transactional
    public Note changeTag(String tag, String newTag) {
        Note changingNote = noteRepository.findNoteByTag(tag);

        changingNote.setTag(Optional.ofNullable(newTag).orElseThrow(
                () -> new FieldIsRequiredException("Field tag is required")
        ));
        changingNote.setChanges(LocalDate.now());

        return noteRepository.save(changingNote);
    }

    @Transactional
    public Note changeName(String tag, String newName) {
        Note changingNote = noteRepository.findNoteByTag(tag);

        if (Optional.ofNullable(newName).isPresent()) {
            changingNote.setName(newName);
            changingNote.setChanges(LocalDate.now());
        }

        return noteRepository.save(changingNote);
    }

    @Transactional
    public Note changeContent(String tag, String newContent) {
        Note changingNote = noteRepository.findNoteByTag(tag);

        if (Optional.ofNullable(newContent).isPresent()) {
            changingNote.setContent(newContent);
            changingNote.setChanges(LocalDate.now());
        }

        return noteRepository.save(changingNote);
    }

    @Transactional
    public Note connectNotes(String startTag, String endTag) {
        Note noteStart = noteRepository.findNoteByTag(startTag);
        Note noteEnd = noteRepository.findNoteByTag(Optional.ofNullable(endTag).orElseThrow(
                () -> new FieldIsRequiredException("Field tag is required")
        ));

        noteRepository.connectNotes(noteEnd.getId(), noteStart.getId());
        noteRepository.connectNotes(noteStart.getId(), noteEnd.getId());
        return noteRepository.findNoteByTag(startTag);
    }

    @Transactional
    public Note deleteNote(String noteTag) {
        Note noteForDelete = noteRepository.findNoteByTag(noteTag);
        noteRepository.delete(noteForDelete);
        return noteForDelete;
    }
}
