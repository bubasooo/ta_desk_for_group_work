package com.ru.tinkoff.tadeskforgroupwork.user;

import com.ru.tinkoff.tadeskforgroupwork.core.configuration.exeptions.FieldIsRequiredException;
import com.ru.tinkoff.tadeskforgroupwork.note.Note;
import com.ru.tinkoff.tadeskforgroupwork.task.Task;
import com.ru.tinkoff.tadeskforgroupwork.user.dto.CreatingUserDto;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User getByNickname(String nickname) {
        return userRepository.findUserByNickname(nickname);
    }

    public User getById(Long users_id) {
        return userRepository.findUserById(users_id);
    }

    @Transactional
    public List<Note> getUsersNotes(String nickname) {
        User user = userRepository.findUserByNickname(nickname);

        return user.getNotes();
    }

    @Transactional
    public List<Task> getUsersTasks(String nickname) {
        User user = userRepository.findUserByNickname(nickname);

        return user.getTasks();
    }

    public User save(CreatingUserDto userDto) {
        User user = User.builder()
                .nickname(Optional.ofNullable(userDto.getNickname()).orElseThrow(
                        () -> new FieldIsRequiredException("Field nickname is required"))
                )
                .role(Optional.ofNullable(userDto.getRole()).orElseThrow(
                        () -> new FieldIsRequiredException("Field role is required"))
                )
                .build();

        return userRepository.save(user);
    }

    @Transactional
    public User changeNickname(String nickname, String newNickName) {
        User changingUser = userRepository.findUserByNickname(nickname);

        if (Optional.ofNullable(newNickName).isPresent())
            changingUser.setNickname(newNickName);

        return userRepository.save(changingUser);
    }

    public User getUserById(Long userId) {
        return userRepository.findUserById(userId);
    }

    @Transactional
    public User changeRole(String nickname, UserRoles newRole) {
        User changingUser = userRepository.findUserByNickname(nickname);

        if(Optional.ofNullable(newRole).isPresent())
            changingUser.setRole(newRole);

        return userRepository.save(changingUser);
    }

    @Transactional
    public User deleteUser(String nickname) {
        User userForDelete = userRepository.findUserByNickname(nickname);

        userRepository.delete(userForDelete);
        return userForDelete;
    }
}
