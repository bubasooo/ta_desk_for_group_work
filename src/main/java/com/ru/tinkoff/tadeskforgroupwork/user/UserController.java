package com.ru.tinkoff.tadeskforgroupwork.user;


import com.ru.tinkoff.tadeskforgroupwork.user.dto.CreatingUserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/{nickname}")
    public User getByNickName(@PathVariable String nickname) {
        return userService.getByNickname(nickname);
    }

    @GetMapping("/byId/{users_id}")
    public User getById(@PathVariable Long users_id) {
        return userService.getUserById(users_id);
    }

    @PostMapping
    public User save(@RequestBody CreatingUserDto userDto) {
        return userService.save(userDto);
    }

    @PatchMapping("/role/{nickname}")
    public User changeRole(@PathVariable String nickname, @RequestBody CreatingUserDto userDto) {
        return userService.changeRole(nickname,userDto.getRole());
    }

    @PatchMapping("/nickname/{currentNickname}")
    public User changeName(@PathVariable String currentNickname, @RequestBody CreatingUserDto userDto) {
        return userService.changeNickname(currentNickname,userDto.getNickname());
    }

    @DeleteMapping("/delete/{nickname}")
    public User deleteUser(@PathVariable String nickname) {
        return userService.deleteUser(nickname);
    }
}

