package com.ru.tinkoff.tadeskforgroupwork.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ru.tinkoff.tadeskforgroupwork.note.Note;
import com.ru.tinkoff.tadeskforgroupwork.task.Task;
import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "users")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRoles role;
    
    @NotNull
    @Column(name = "nickname", length = 20)
    private String nickname;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "usersNote", cascade = CascadeType.REMOVE)
    private List<Note> notes;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "usersTask",cascade = CascadeType.REMOVE)
    private List<Task> tasks;
}