package com.ru.tinkoff.tadeskforgroupwork.user.dto;


import com.ru.tinkoff.tadeskforgroupwork.user.UserRoles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatingUserDto {
    private String nickname;
    private UserRoles role;
}
