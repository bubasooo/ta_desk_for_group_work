package com.ru.tinkoff.tadeskforgroupwork.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByNickname(String nickname);

    User findUserById(Long userId);
}
