package com.ru.tinkoff.tadeskforgroupwork.form;

import com.ru.tinkoff.tadeskforgroupwork.desk.Desk;
import com.ru.tinkoff.tadeskforgroupwork.desk.DeskService;
import com.ru.tinkoff.tadeskforgroupwork.note.NoteService;
import com.ru.tinkoff.tadeskforgroupwork.task.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/form")
@RequiredArgsConstructor
public class FormController {
    private FormService formService;

    @GetMapping
    public List<Form> findAll() {
        return formService.findAll();
    }

}
