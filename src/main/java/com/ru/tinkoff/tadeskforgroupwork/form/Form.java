package com.ru.tinkoff.tadeskforgroupwork.form;

public interface Form {
    String getTag();
}
