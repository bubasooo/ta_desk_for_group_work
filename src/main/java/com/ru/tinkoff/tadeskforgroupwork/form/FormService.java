package com.ru.tinkoff.tadeskforgroupwork.form;

import com.ru.tinkoff.tadeskforgroupwork.note.NoteService;
import com.ru.tinkoff.tadeskforgroupwork.task.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FormService {
    private final TaskService taskService;
    private final NoteService noteService;

    List<Form> findAll() {
        return List.of(
               taskService.findAll(), noteService.findAll()
        ).stream().flatMap((form) -> form.stream()).collect(Collectors.toList());
    }

    List<Form> findByTag(String tag) {
        return List.of(
                taskService.findByTag(tag), noteService.findByTag(tag)
        );
    }
}
