package com.ru.tinkoff.tadeskforgroupwork.desk;


import com.ru.tinkoff.tadeskforgroupwork.desk.dto.CreatingDeskDto;
import com.ru.tinkoff.tadeskforgroupwork.note.Note;
import com.ru.tinkoff.tadeskforgroupwork.task.Task;
import com.ru.tinkoff.tadeskforgroupwork.user.dto.CreatingUserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/desk")
@RequiredArgsConstructor
public class DeskController {

    private final DeskService deskService;

    @GetMapping
    public List<Desk> findAll() {
        return deskService.findAll();
    }

    @GetMapping("/{deskId}/get_tasks")
    public List<Task> getTasks(@PathVariable Long deskId) {
        return deskService.getTasks(deskId);
    }

    @GetMapping("/{deskId}")
    public Desk findDeskById(@PathVariable Long deskId) {
        return deskService.findDeskById(deskId);
    }

    @PostMapping
    public Desk save(@RequestBody CreatingDeskDto deskDto) {
        return deskService.save(deskDto);
    }

    @PatchMapping("/update/status/{deskId}")
    public Desk updateStatus(@PathVariable Long deskId, @RequestBody CreatingDeskDto deskDto) {
        return deskService.updateStatus(deskId, deskDto.getStatus());
    }

    @PatchMapping("/edit/name/{deskId}")
    public Desk changeName(@PathVariable Long deskId, @RequestBody CreatingDeskDto deskDto) {
        return deskService.changeName(deskId, deskDto.getName());
    }

    @PatchMapping("/edit/description/{deskId}")
    public Desk changeDescription(@PathVariable Long deskId, @RequestBody CreatingDeskDto deskDto) {
        return deskService.changeDescription(deskId, deskDto.getDescription());
    }

    @DeleteMapping("/delete/{deskId}")
    public Desk deleteDesk(@PathVariable Long deskId) {
        return deskService.deleteDesk(deskId);
    }
}
