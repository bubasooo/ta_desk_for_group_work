package com.ru.tinkoff.tadeskforgroupwork.desk;


import com.ru.tinkoff.tadeskforgroupwork.core.configuration.exeptions.FieldIsRequiredException;
import com.ru.tinkoff.tadeskforgroupwork.desk.dto.CreatingDeskDto;
import com.ru.tinkoff.tadeskforgroupwork.task.Task;
import lombok.RequiredArgsConstructor;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DeskService {
    private final ModelMapper modelMapper;
    private final DeskRepository deskRepository;

    public List<Desk> findAll() {
        return deskRepository.findAll();
    }

    public Desk findDeskById(Long deskId) {
        return deskRepository.findDeskById(deskId);
    }


    public List<Task> getTasks(Long deskId) {
        Desk desk = deskRepository.findDeskById(deskId);

        return desk.getTasks();
    }

    public Desk save(CreatingDeskDto deskDto) {

        Desk desk = Desk.builder()
                .name(Optional.ofNullable(deskDto.getName()).orElse("New desk"))
                .description(Optional.ofNullable(deskDto.getDescription()).orElse("No description"))
                .status(Optional.ofNullable(deskDto.getStatus()).orElseThrow(
                        () -> new FieldIsRequiredException("Field status is required")
                ))
                .creates(LocalDate.now())
                .changes(LocalDate.now())
                .build();

        return deskRepository.save(desk);
    }

    public Desk updateStatus(Long deskId, DeskStatus newStatus) {
        Desk deskToUpdate = deskRepository.findDeskById(deskId);

        deskToUpdate.setStatus(Optional.ofNullable(newStatus).orElseThrow(
                () -> new FieldIsRequiredException("Field status is required")
        ));
        deskToUpdate.setChanges(LocalDate.now());

        return deskRepository.saveAndFlush(deskToUpdate);
    }

    public Desk changeName(Long deskId, String newName) {
        Desk deskToUpdate = deskRepository.findDeskById(deskId);

        if (Optional.ofNullable(newName).isPresent()) {
            deskToUpdate.setName(newName);
            deskToUpdate.setChanges(LocalDate.now());
        }

        return deskRepository.save(deskToUpdate);
    }

    public Desk changeDescription(Long deskId, String newDescription) {
        Desk deskToUpdate = deskRepository.findDeskById(deskId);

        if (Optional.ofNullable(newDescription).isPresent()) {
            deskToUpdate.setDescription(newDescription);
            deskToUpdate.setChanges(LocalDate.now());
        }

        return deskRepository.save(deskToUpdate);
    }

    public Desk deleteDesk(Long deskId) {
        Desk deskToDelete = deskRepository.findDeskById(deskId);

        deskRepository.delete(deskToDelete);

        return deskToDelete;
    }
}
