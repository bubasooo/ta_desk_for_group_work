package com.ru.tinkoff.tadeskforgroupwork.desk;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ru.tinkoff.tadeskforgroupwork.task.Task;
import jakarta.persistence.*;
import lombok.*;


import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "desk")
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Desk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "name", length = 20)
    private String name;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "creates")
    private LocalDate creates;

    @NotNull
    @Column(name = "changes")
    private LocalDate changes;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private DeskStatus status;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy="desk", cascade = CascadeType.REMOVE)
    private List<Task> tasks;

}