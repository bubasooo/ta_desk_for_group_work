package com.ru.tinkoff.tadeskforgroupwork.desk.dto;

import com.ru.tinkoff.tadeskforgroupwork.desk.DeskStatus;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



import java.time.LocalDate;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingDeskDto {
    private String name;
    private String description;
    private DeskStatus status;

}
