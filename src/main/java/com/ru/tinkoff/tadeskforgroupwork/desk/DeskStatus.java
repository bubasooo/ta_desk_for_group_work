package com.ru.tinkoff.tadeskforgroupwork.desk;

public enum DeskStatus {
    TODO, IN_PROGRESS, DONE
}
