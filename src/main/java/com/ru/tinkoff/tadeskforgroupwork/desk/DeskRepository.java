package com.ru.tinkoff.tadeskforgroupwork.desk;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeskRepository extends JpaRepository<Desk,Long> {
    Desk findDeskById(Long deskId);
}
