create table users (
    id bigserial primary key,
    "role" text not null,
    nickname varchar(20) unique
);


create table note (
    id bigserial primary key,
    "name" varchar(20) default ('New Note'),
    "content" text default ('No content'),
    tag varchar(20) unique ,
    creates date not null,
    "changes" date not null,
    users_id bigint not null,

    foreign key(users_id) references users(id)
);

create table note_note (
    note_start_id bigint,
    note_end_id bigint,

    primary key(note_start_id, note_end_id),
    foreign key(note_start_id) references note(id),
    foreign key(note_end_id) references note(id)
);



create table desk (
    id bigserial primary key,
    "name" varchar(20) default ('New Desk'),
    description text default ('No descrtiption'),
    creates date not null,
    "changes" date not null,
    status text not null
);


create table task (
    id bigserial primary key,
    "name" text default ('New Task'),
    description text default ('No Description'),
    creates date not null,
    "changes" date not null,
    tag varchar(20) unique, --??
    desk_id bigint not null,
    users_id bigint not null,

    foreign key(desk_id) references desk(id),
    foreign key(users_id) references users(id)
);




